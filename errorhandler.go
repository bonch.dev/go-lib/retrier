package retrier

type RetryErrorHandler func(retryCounter, maxRetryCount int, err error)

func DefaultErrorHandler(logger RetryLogger) RetryErrorHandler {
	return func(retryCounter, maxRetryCount int, err error) {
		logger.Printf("Started new retry (%d / %d). Failed to process func in retrier: %v", retryCounter, maxRetryCount, err)
	}
}
