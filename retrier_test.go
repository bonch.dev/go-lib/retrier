package retrier_test

import (
	"context"
	"sync"
	"testing"
	"time"

	"github.com/pkg/errors"
	"github.com/stretchr/testify/assert"

	"gitlab.com/bonch.dev/go-lib/retrier"
)

var (
	errFoo = errors.New("FOO")
	errBar = errors.New("BAR")
	errBaz = errors.New("BAZ")
	i      int
)

func genWork(returns []error) func() error {
	i = 0
	return func() error {
		i++
		if i > len(returns) {
			return nil
		}
		return returns[i-1]
	}
}

func genWorkWithCtx() func(ctx context.Context) error {
	i = 0
	return func(ctx context.Context) error {
		select {
		case <-ctx.Done():
			return errFoo
		default:
			i++
		}
		return errBar
	}
}

func TestFailedGoroutines(t *testing.T) {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	retryCount := 10

	r := retrier.NewRetrier(retryCount)

	wg := sync.WaitGroup{}
	wg.Add(1)

	go func(wg *sync.WaitGroup) {
		r.RunCtx(ctx, genWorkWithCtx())

		wg.Done()
	}(&wg)

	wg.Wait()

	assert.Equal(t, retryCount+1, i)
}

func TestRetrier(t *testing.T) {
	r := retrier.NewRetrier(3).WithClassifier(retrier.WhitelistClassifier{errFoo})

	err := r.Run(genWork([]error{errFoo, errFoo}))

	assert.Equal(t, nil, err)
	assert.Equal(t, 3, i, "run wrong number of times")

	err = r.Run(genWork([]error{errFoo, errBar}))

	assert.Equal(t, errBar, err)
	assert.Equal(t, 2, i, "run wrong number of times")

	err = r.Run(genWork([]error{errBar, errBaz}))

	assert.Equal(t, errBar, err)
	assert.Equal(t, 1, i, "run wrong number of times")
}

func TestRetrierCtx(t *testing.T) {
	ctx, cancel := context.WithCancel(context.Background())

	r := retrier.NewRetrier(1).WithClassifier(retrier.WhitelistClassifier{})

	err := r.RunCtx(ctx, genWorkWithCtx())

	assert.Equal(t, errBar, err)
	assert.Equal(t, 1, i, "run wrong number of times")

	cancel()

	err = r.RunCtx(ctx, genWorkWithCtx())

	assert.Equal(t, errFoo, err, "context must be cancelled")
	assert.Equal(t, 0, i, "run wrong number of times")
}

func TestRetrierJitter(t *testing.T) {
	r := retrier.NewRetrier(-1).WithMode(retrier.ExponentialMode)

	assert.Equal(t, 2*time.Millisecond, r.CalcSleep(1))
	assert.Equal(t, 4*time.Millisecond, r.CalcSleep(2))

	r.WithMode(retrier.JitterMode(0.25, retrier.ExponentialMode))
	assert.InDelta(t, 2*time.Millisecond, r.CalcSleep(1), .5*float64(time.Millisecond))
	assert.InDelta(t, 4*time.Millisecond, r.CalcSleep(2), 1*float64(time.Millisecond))
}
