package retrier

import (
	"context"
	"time"
)

const DefaultDuration = time.Millisecond
const InfiniteRetryCount = -1

type Retrier struct {
	class        Classifier
	mode         Mode
	backoff      time.Duration
	logger       RetryLogger
	errorHandler RetryErrorHandler
	retryCount   int
}

func NewRetrier(retryCount int) *Retrier {
	return &Retrier{
		retryCount:   retryCount,
		class:        DefaultClassifier{},
		mode:         DefaultMode,
		backoff:      DefaultDuration,
		logger:       DefaultLogger,
		errorHandler: DefaultErrorHandler(DefaultLogger),
	}
}

func NewInfiniteRetrier() *Retrier {
	return NewRetrier(InfiniteRetryCount)
}

func (r *Retrier) WithBackoff(backoff time.Duration) *Retrier {
	r.backoff = backoff

	return r
}

func (r *Retrier) WithClassifier(c Classifier) *Retrier {
	r.class = c

	return r
}

func (r *Retrier) WithMode(mode Mode) *Retrier {
	r.mode = mode

	return r
}

func (r *Retrier) WithLogger(logger RetryLogger) *Retrier {
	r.logger = logger

	return r
}

func (r *Retrier) WithErrorHandler(errorHandler RetryErrorHandler) *Retrier {
	r.errorHandler = errorHandler

	return r
}

func (r *Retrier) Run(work func() error) error {
	return r.RunCtx(context.Background(), func(ctx context.Context) error {
		return work()
	})
}

// RunCtx executes the given work function, then classifies its return value based on the classifier used
// to construct the Retrier. If the result is Succeed or Fail, the return value of the work function is
// returned to the caller. If the result is Retry, then Run sleeps according to the its backoff policy
// before retrying. If the total number of retries is exceeded then the return value of the work function
// is returned to the caller regardless.
func (r *Retrier) RunCtx(ctx context.Context, work func(ctx context.Context) error) error {
	retries := 0

	for {
		ret := work(ctx)

		switch r.class.Classify(ret) {
		case Succeed, Fail:
			return ret
		case Retry:
			if retries >= r.retryCount && r.retryCount != -1 {
				return ret
			}

			timeout := time.After(r.CalcSleep(retries))
			if err := r.sleep(ctx, timeout); err != nil {
				return err
			}

			retries++

			if r.errorHandler != nil {
				r.errorHandler(retries, r.retryCount, ret)
			}
		}
	}
}

func (r *Retrier) sleep(ctx context.Context, t <-chan time.Time) error {
	select {
	case <-t:
		return nil
	case <-ctx.Done():
		return ctx.Err()
	}
}

func (r *Retrier) CalcSleep(i int) time.Duration {
	return r.mode(i, r.backoff)
}
