package retrier

import (
	"crypto/rand"
	"math"
	"math/big"
	"time"
)

type Mode func(retryCounter int, backoff time.Duration) time.Duration

const twoPower = 2

var (
	DefaultMode Mode = func(retryCounter int, backoff time.Duration) time.Duration {
		return backoff
	}

	ArithmeticMode Mode = func(retryCounter int, backoff time.Duration) time.Duration {
		return time.Duration(retryCounter) * backoff
	}

	ExponentialMode Mode = func(retryCounter int, backoff time.Duration) time.Duration {
		return time.Duration(math.Pow(twoPower, float64(retryCounter))) * backoff
	}
)

func JitterMode(jitterCoef float64, mode Mode) Mode {
	random := randomFloat64()

	return func(retryCounter int, backoff time.Duration) time.Duration {
		return time.Duration(float64(mode(retryCounter, backoff)) * (1 + (random*2-1)*jitterCoef))
	}
}

func randomFloat64() float64 {
	number, _ := rand.Int(rand.Reader, big.NewInt(math.MaxInt64))

	return float64(number.Int64()) / math.MaxInt64
}
