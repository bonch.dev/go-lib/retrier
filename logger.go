package retrier

import (
	"log"
)

type RetryLogger interface {
	Printf(format string, args ...interface{})
}

var DefaultLogger = log.Default()
