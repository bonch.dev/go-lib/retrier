package retrier_test

import (
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/bonch.dev/go-lib/retrier"
)

func TestDefaultClassifier(t *testing.T) {
	c := retrier.DefaultClassifier{}

	assert.Equal(t, retrier.Succeed, c.Classify(nil), "default misclassified nil")
	assert.Equal(t, retrier.Retry, c.Classify(errFoo), "default misclassified foo")
	assert.Equal(t, retrier.Retry, c.Classify(errBar), "default misclassified bar")
	assert.Equal(t, retrier.Retry, c.Classify(errBaz), "default misclassified baz")
}

func TestWhitelistClassifier(t *testing.T) {
	c := retrier.WhitelistClassifier{errFoo, errBar}

	assert.Equal(t, retrier.Succeed, c.Classify(nil), "whitelist misclassified nil")
	assert.Equal(t, retrier.Retry, c.Classify(errFoo), "whitelist misclassified foo")
	assert.Equal(t, retrier.Retry, c.Classify(errBar), "whitelist misclassified bar")
	assert.Equal(t, retrier.Fail, c.Classify(errBaz), "whitelist misclassified baz")
}

func TestBlacklistClassifier(t *testing.T) {
	c := retrier.BlacklistClassifier{errBar}

	assert.Equal(t, retrier.Succeed, c.Classify(nil), "blacklist misclassified nil")
	assert.Equal(t, retrier.Retry, c.Classify(errFoo), "blacklist misclassified foo")
	assert.Equal(t, retrier.Fail, c.Classify(errBar), "blacklist misclassified bar")
	assert.Equal(t, retrier.Retry, c.Classify(errBaz), "blacklist misclassified baz")
}
